from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField, TextAreaField, SelectField, BooleanField
from wtforms.validators import DataRequired


class PostForm(FlaskForm):
    title = StringField('Заголовок', validators=[DataRequired()])
    transport_type = SelectField('Вид траспорта',
                                 choices=[('манипулятор', 'манипулятор'),
                                          ('автобетононасос', 'автобетононасос'),
                                          ('автобетоносмеситель', 'автобетоносмеситель'),
                                          ('эвакуатор', 'эвакуатор'), ('экскаватор', 'экскаватор'),
                                          ('автовышка', 'автовышка'), ('автокран', 'автокран'),
                                          ('мусоровоз', 'мусоровоз'), ('бензовоз', 'бензовоз'),
                                          ('погрузчик-манипулятор', 'погрузчик-манипулятор'),
                                          ('трактор', 'трактор'), ('цементовоз', 'цементовоз'),
                                          ('погрузчик вилочный', 'погрузчик вилочный')], validators=[DataRequired()])

    city = SelectField('Город', choices=[('Алматы', 'Алматы'), ('Нур-Султан', 'Нур-Султан'),
                                         ('Шымкент', 'Шымкент'), ('Тараз', 'Тараз'), ('Актау', 'Актау'),
                                         ('Актобе', 'Актобе'), ('Караганда', 'Караганда'),
                                         ('Павлодар', 'Павлодар'), ('Усть-Каменогорск', 'Усть-Каменогорск'),
                                         ('Семей', 'Семей'), ('Атырау', 'Атырау'),
                                         ('Петропавловск', 'Петропавловск'), ('Уральск,', 'Уральск,'),
                                         ('Талдыкорган', 'Талдыкорган'),
                                         ('Кызылорда', 'Кызылорда'), ('Кокшетау', 'Кокшетау')],
                       validators=[DataRequired()])
    description = TextAreaField('Описание объявления', validators=[DataRequired()])

    available = BooleanField('Свободен')

    image = FileField('Загрузите фото',
                        validators=[
                            FileAllowed(['jpg', 'png', 'jpeg'], 'Файл не имеет утвержденного расширения: jpg, png.')])

    submit = SubmitField('Опубликовать')