from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from pitak import db
from pitak.models import Post, Comment
from pitak.posts.forms import PostForm
from pitak.posts.utils import save_image

posts = Blueprint('posts', __name__)


@posts.route("/")
# @login_required
def poster():
    q = request.args.get('q')

    page = request.args.get('page')

    if page and page.isdigit():
        page = int(page)
    else:
        page = 1

    if q:
        posts = Post.query.filter(Post.title.contains(q) | Post.description.contains(q) |
                                  Post.city.contains(q) | Post.transport_type.contains(q))
    else:
        # posts = Post.query.all()
        posts = Post.query.order_by(Post.date_posted.desc())

    pages = posts.paginate(page=page, per_page=5)

    return render_template('posts.html', posts=posts, pages=pages, title='Объявления')


@posts.route("/post/new", methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        if form.image.data:
            image_file = save_image(form.image.data)
            form.image.data = image_file
        post = Post(title=form.title.data, transport_type=form.transport_type.data,
                    city=form.city.data, description=form.description.data,
                    image_file=form.image.data, available=form.available.data,
                    author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Ваше объявление создано!', 'success')
        return redirect(url_for('posts.poster'))
    return render_template('create_post.html', title='Подать объявление',
                           form=form, legend='Подать объявление')


@posts.route("/my-posts")
def my_posts():
    posts = current_user.posts
    return render_template('my_posts.html', posts=posts, title='Мои объявления')


@posts.route("/my-posts/<int:post_id>")
def my_post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)


@posts.route("/my-posts/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_my_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.transport_type = form.transport_type.data
        post.city = form.city.data
        post.description = form.description.data
        post.available = form.available.data
        db.session.commit()
        flash('Объявление успешно отредактирован!', 'success')
        return redirect(url_for('posts.my_post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.transport_type.data = post.transport_type
        form.city.data = post.city
        form.description.data = post.description
    return render_template('create_post.html', title='Редактировать объявление', form=form,
                           legend='Редактировать объявление')


@posts.route("/my-posts/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_my_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Объявление успешно удален!', 'success')
    return redirect(url_for('posts.my_posts'))


@posts.route("/post/<int:post_id>", methods=['GET', 'POST'])
def post(post_id):
    post = Post.query.get_or_404(post_id)
    comments = Comment.query.filter_by(post_id=post.id).all()
    post.views += 1

    if request.method == 'POST':
        message = request.form.get('message')
        comment = Comment(fullname=current_user.fullname, email=current_user.email,
                          image_file=current_user.image_file, message=message, post_id=post.id)
        db.session.add(comment)
        flash('Ваш комментарий отправлен!', 'success')
        post.comments += 1
        db.session.commit()
        return redirect(request.url)
    return render_template('post.html', title=post.title, post=post, comments=comments)