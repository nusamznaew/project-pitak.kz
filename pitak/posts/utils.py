import os
import secrets
from PIL import Image
from flask import current_app


def save_image(form_image):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_image.filename)
    image_file_name = random_hex + f_ext
    image_path = os.path.join(current_app.root_path, 'static/post_pics', image_file_name)

    output_size = (500, 500)
    i = Image.open(form_image)
    i.thumbnail(output_size)
    i.save(image_path)

    return image_file_name


# def save_post_pic(form_image):
#
#     print(form_image)
#     for pic in form_image:
#         print(pic)
#         random_hex = secrets.token_hex(8)
#         _, f_ext = os.path.splitext(pic.filename)
#         image_file_name = random_hex + f_ext
#         # print(image_file_name)
#         image_path = os.path.join(current_app.root_path, 'static/post_pics', image_file_name)
#         # extension = pic.split('.')[1]
#         # image_file_name = random_hex+extension
#         # print(extension)
#         # image_path = os.path.join(current_app.root_path, 'static/post_pics', pic)
#         # print(image_path)
#
#         output_size = (125, 125)
#         i = Image.open(pic)
#         i.thumbnail(output_size)
#         i.save(image_path)
#
#         return image_file_name
