import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from pitak import mail


def save_image(form_image):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_image.filename)
    image_file_name = random_hex + f_ext
    image_path = os.path.join(current_app.root_path, 'static/profile_pics', image_file_name)

    output_size = (125, 125)
    i = Image.open(form_image)
    i.thumbnail(output_size)
    i.save(image_path)

    return image_file_name


def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Запрос на сброс пароля',
                  sender='noreply@demo.com',
                  recipients=[user.email])
    msg.body = f'''Чтобы сбросить пароль, перейдите по следующей ссылке:
{url_for('users.reset_token', token=token, _external=True)}

Если вы не отправили этот запрос, просто проигнорируйте это письмо, и никакие изменения не будут внесены.
'''
    mail.send(msg)