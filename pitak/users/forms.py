from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flask_login import current_user
from pitak.models import User


class RegistrationForm(FlaskForm):
    fullname = StringField('ФИО',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('E-mail',
                        validators=[DataRequired(), Email()])
    phone_number = StringField('Номер',
                               validators=[DataRequired()])
    password = PasswordField('Придумайте пароль',
                             validators=[DataRequired()])
    confirm_password = PasswordField('Подтвердите пароль',
                                     validators=[DataRequired(), EqualTo('password')])
    transporter = BooleanField('Регистрируюсь в качестве грузоперезовчика*')

    submit = SubmitField('Зарегистрироваться')

    @staticmethod
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Этот адрес электронной почты занят. Пожалуйста, выберите другой.')

    @staticmethod
    def validate_phone_number(self, phone_number):
        user = User.query.filter_by(phone_number=phone_number.data).first()
        if user:
            raise ValidationError('Этот телефонный номер занят. Пожалуйста, выберите другой.')


class LoginForm(FlaskForm):
    email = StringField('E-mail',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Пароль',
                             validators=[DataRequired()])
    remember = BooleanField('Запомнить меня')

    submit = SubmitField('Войти')


class UpdateAccountForm(FlaskForm):
    fullname = StringField('ФИО',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('E-mail',
                        validators=[DataRequired(), Email()])
    phone_number = StringField('Номер',
                               validators=[DataRequired()])
    image = FileField('Фото профиля',
                        validators=[FileAllowed(['jpg', 'png', 'jpeg'], 'Файл не имеет утвержденного расширения: jpg, png.')])
    submit = SubmitField('Изменить')

    @staticmethod
    def validate_fullname(self, fullname):
        if fullname.data != current_user.fullname:
            user = User.query.filter_by(email=fullname.data).first()
            if user:
                raise ValidationError('Этот адрес электронной почты занят. Пожалуйста, выберите другой.')

    @staticmethod
    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('Этот адрес электронной почты занят. Пожалуйста, выберите другой.')

    @staticmethod
    def validate_phone_number(self, phone_number):
        if phone_number.data != current_user.phone_number:
            user = User.query.filter_by(phone_number=phone_number.data).first()
            if user:
                raise ValidationError('Этот телефонный номер занят. Пожалуйста, выберите другой.')


class RequestResetForm(FlaskForm):
    email = StringField('E-mail',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Отправить')

    @staticmethod
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('Нет никакой учетной записи с этой электронной почтой.'
                                  ' Сначала вы должны зарегистрироваться.')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Придумайте пароль',
                             validators=[DataRequired()])
    confirm_password = PasswordField('Подтвердите пароль',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Восстановить')
